Given("{var} ← {color}") do |name, color|
  instance_variable_set(name, color)
end

Then("{var} + {var} = {color}") do |name1, name2, color|
  assert_equal(instance_variable_get(name1) + instance_variable_get(name2), color)
end

Then("{var} - {var} = {color}") do |name1, name2, color|
  assert_equal(instance_variable_get(name1) - instance_variable_get(name2), color)
end

Then("I multiply a color {var} by a scalar {int} = {color}") do |name, scalar, color|
  assert_equal(instance_variable_get(name) * scalar, color)
end

Then("I multiply two colors {var} * {var} = {color}") do |name1, name2, color|
  assert_equal(instance_variable_get(name1) * instance_variable_get(name2), color)
end

Then("c.{word} = {float}") do |attribute, value|
  instance_variable_set(:@color_value, @c.send(attribute))
  assert_equal(Rayz::Util.equal?(@color_value, value), true)
end
