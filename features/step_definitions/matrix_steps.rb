Given("the following matrix {var}:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  instance_variable_set(name, Rayz::Matrix[row0, row1, row2, row3])
end

Given("the following (4x4) matrix {var}:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  instance_variable_set(name, Rayz::Matrix[row0, row1, row2, row3])
end

Given("the following 3x3 matrix {var}:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  instance_variable_set(name, Rayz::Matrix[row0, row1, row2])
end

Given("the following 2x2 matrix {var}:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  instance_variable_set(name, Rayz::Matrix[row0, row1])
end

Then("{var}[{int},{int}] = {float}") do |name, x, y, value|
  assert_equal(instance_variable_get(name)[x, y], value)
end

Then("matrix {var} = matrix {var}") do |name1, name2|
  assert_equal(instance_variable_get(name1), instance_variable_get(name1))
end

Then("matrix {var} != matrix {var}") do |name1, name2|
  refute_equal(instance_variable_get(name1), instance_variable_get(name2))
end

Then("{var} * {var} is the following 4x4 matrix:") do |name1, name2, table|
  # table is a Cucumber::MultilineArgument::DataTable
  instance_variable_set(:@calculated_result, instance_variable_get(name1) * instance_variable_get(name2))
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  instance_variable_set(:@expected_result, Rayz::Matrix[row0, row1, row2, row3])
  assert_equal(@calculated_result, @expected_result)
end

Then("{var} * {var} = {tuple}") do |name1, name2, tuple|
  assert_equal(instance_variable_get(name1).times_tuple(instance_variable_get(name2)), tuple)
end

Then("{var} * identity_matrix = {var}") do |name1, name2|
  assert_equal(instance_variable_get(name1) * Rayz::Matrix.identity(4), instance_variable_get(name2))
end

Then("identity_matrix * {var} = {var}") do |name1, name2|
  assert_equal(Rayz::Matrix.identity(4).times_tuple(instance_variable_get(name2)), instance_variable_get(name2))
end

Then("transpose\\({var}) is the following matrix:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  @expected = Rayz::Matrix[row0, row1, row2, row3]
  instance_variable_set(:@x, instance_variable_get(name))
  assert_equal(@x.transpose, @expected)
end

Given("{var} ← transpose\\(identity_matrix)") do |name|
  instance_variable_set(name, Rayz::Matrix.identity(4).transpose)
end

Then("{var} = identity_matrix") do |name|
  assert_equal(instance_variable_get(name), Rayz::Matrix.identity(4))
end

Then("determinant\\({var}) = {int}") do |name, int|
  assert_equal(instance_variable_get(name).determinant, int)
end

Then("submatrix\\({var}, {int}, {int}) is the following 2x2 matrix:") do |name, row, column, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  instance_variable_set(:@expected_matrix, Rayz::Matrix[row0, row1])
  assert_equal(@expected_matrix, instance_variable_get(name).submatrix(row, column))
end

Then("submatrix\\({var}, {int}, {int}) is the following 3x3 matrix:") do |name, row, column, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  instance_variable_set(:@expected_matrix, Rayz::Matrix[row0, row1, row2])
  assert_equal(@expected_matrix, instance_variable_get(name).submatrix(row, column))
end

Given("{var} ← submatrix\\({var}, {int}, {int})") do |name1, name2, row, column|
  instance_variable_set(name1, instance_variable_get(name2).submatrix(row, column))
end

Then("minor\\({var}, {int}, {int}) = {int}") do |name, row, column, int|
  instance_variable_set(:@minor, instance_variable_get(name).mynor(row, column))
  assert_equal(@minor, int)
end

Then("cofactor\\({var}, {int}, {int}) = {int}") do |name, row, column, int|
  assert_equal(instance_variable_get(name).cofactor(row, column), int)
end

Then("{var} is invertible") do |name|
  assert_equal(instance_variable_get(name).invertible?, true)
end

Then("{var} is not invertible") do |name|
  refute_equal(instance_variable_get(name).invertible?, true)
end

Given("{var} ← inverse\\({var})") do |name1, name2|
  instance_variable_set(name1, instance_variable_get(name2).inverse)
end

Then("{var}[{int},{int}] = {int}\\/{int}") do |name, int, int2, int3, int4|
  instance_variable_set(:@a, instance_variable_get(name)[int, int2])
  @b = int3.to_f / int4.to_f
  result = Rayz::Util.equal?(@a, @b)
  assert_equal(result, true)
end

Then("{var} is the following 4x4 matrix:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  @expected_result = Rayz::Matrix[row0, row1, row2, row3]
  assert_equal(instance_variable_get(name), @expected_result)
end

Then("inverse of \\({var}\\) is the following 4x4 matrix:") do |name, table|
  # table is a Cucumber::MultilineArgument::DataTable
  row0 = table.to_hash[0].map(&:to_f)
  row1 = table.to_hash[1].map(&:to_f)
  row2 = table.to_hash[2].map(&:to_f)
  row3 = table.to_hash[3].map(&:to_f)
  @expected_result = Rayz::Matrix[row0, row1, row2, row3]
  assert_equal(instance_variable_get(name).inverse, @expected_result)
end

Given("{var} ← {var} * {var}") do |c, a, b|
  instance_variable_set(c, instance_variable_get(a) * instance_variable_get(b))
end

Then("{var} * inverse\\({var}\\) = {var}") do |c, b, a|
  assert_equal(instance_variable_get(c) * instance_variable_get(b).inverse, instance_variable_get(a))
end
