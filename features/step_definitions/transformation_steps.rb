Given("{var} ← {translation}") do |name, translation|
  instance_variable_set(name, translation)
end

Then("{var} * {var} = {point}") do |t_name, p_name, point|
  assert_equal(instance_variable_get(t_name).times_tuple(instance_variable_get(p_name)), point)
end

Then("{var} * {var} = {var}") do |t_name, v_name1, v_name2|
  assert_equal(instance_variable_get(t_name).times_tuple(instance_variable_get(v_name1)), instance_variable_get(v_name2))
end

Given("{var} ← {scaling}") do |t_name, scaling|
  instance_variable_set(t_name, scaling)
end

Then("{var} * {var} = {vector}") do |t_name, v_name, vector|
  assert_equal(instance_variable_get(t_name).times_tuple(instance_variable_get(v_name)), vector)
end

Given("{var} ← {rotation}\\(π \\/ {int}\\)") do |name, rotation, number|
  instance_variable_set(name, Rayz::Matrix.public_send(rotation, Math::PI / number.to_f))
end

Then("{var} * {var} = point\\({int}, √{int}\\/{int}, √{int}\\/{int}\\)") do |rotation, point_name, a, b, c, d, e|
  n1 = Math.sqrt(b.to_f) / c.to_f
  n2 = Math.sqrt(d.to_f) / e.to_f
  p = Rayz::Point.new(x: a.to_f, y: n1, z: n2)
  result = instance_variable_get(rotation).times_point(instance_variable_get(point_name))
  assert_equal(result, p)
end

Then("{var} * {var} = point\\({int}, √{int}\\/{int}, -√{int}\\/{int}\\)") do |rotation, point_name, a, b, c, d, e|
  n1 = Math.sqrt(b.to_f) / c.to_f
  n2 = -(Math.sqrt(d.to_f) / e.to_f)
  p = Rayz::Point.new(x: a.to_f, y: n1, z: n2)
  result = instance_variable_get(rotation).times_point(instance_variable_get(point_name))
  assert_equal(result, p)
end
