Given("{var} ← {tuple}") do |name, tuple|
  instance_variable_set(name, tuple)
end

Given("{var} ← {point}") do |name, point|
  instance_variable_set(name, point)
end

Given("{var} ← {vector}") do |name, vector|
  instance_variable_set(name, vector)
end

When("{var} ← normalize\\({var}\\)") do |name1, name2|
  instance_variable_set(name1, instance_variable_get(name2).normalize)
end

Then("{var} + {var} = {tuple}") do |name1, name2, tuple|
  assert_equal(instance_variable_get(name1) + instance_variable_get(name2), tuple)
end

Then("{var} - {var} = {vector}") do |name1, name2, vector|
  assert_equal(instance_variable_get(name1) - instance_variable_get(name2), vector)
end

Then("{var} - {var} = {point}") do |name1, name2, point|
  assert_equal(instance_variable_get(name1) - instance_variable_get(name2), point)
end

Then("{var} = {tuple}") do |name, tuple|
  assert_equal(instance_variable_get(name), tuple)
end

Then("negated {var} = {tuple}") do |name, tuple|
  assert_equal(-instance_variable_get(name), tuple)
end

Then("{var} * {float} = {tuple}") do |name, scalar, tuple|
  assert_equal(instance_variable_get(name) * scalar, tuple)
end

Then("{var} \\/ {float} = {tuple}") do |name, scalar, tuple|
  assert_equal(instance_variable_get(name) / scalar, tuple)
end

Then("magnitude\\({var}\\) = {int}") do |name, scalar|
  assert_equal(instance_variable_get(name).magnitude, scalar)
end

Then("magnitude\\({var}\\) = √{int}") do |name, scalar|
  assert_equal(Rayz::Util.equal?(instance_variable_get(name).magnitude, Math.sqrt(scalar)), true)
end

Then("normalize\\({var}\\) = {vector}") do |name, vector|
  assert_equal(instance_variable_get(name).normalize, vector)
end

Then("dot\\({var}, {var}\\) = {int}") do |name1, name2, scalar|
  assert_equal(instance_variable_get(name1).dot(instance_variable_get(name2)), scalar)
end

Then("cross\\({var}, {var}\\) = {vector}") do |name1, name2, vector|
  assert_equal(instance_variable_get(name1).cross(instance_variable_get(name2)), vector)
end

Then("a.{word} = {float}") do |attribute, value|
  result = Rayz::Util.equal?(@a.send(attribute), value)
  assert_equal(result, true)
end

And("a is a point") do
  assert_equal(@a.point?, true)
end

And("a is not a vector") do
  assert_equal(@a.vector?, false)
end

And("a is not a point") do
  assert_equal(@a.point?, false)
end

And("a is a vector") do
  assert_equal(@a.vector?, true)
end
