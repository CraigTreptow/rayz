Given("{var} ← {canvas}") do |name, canvas|
  instance_variable_set(name, canvas)
end

Then("every pixel of {var} is {color}") do |name, color|
  instance_variable_get(name).height.times do |height_index|
    instance_variable_get(name).width.times do |width_index|
      result = instance_variable_get(name).grid[width_index][height_index] == color
      assert_equal(result, true)
    end
  end
end

Then("write_pixel\\({var}, {int}, {int}, {var}\\)") do |name, x, y, color|
  instance_variable_get(name).write_pixel(x: x, y: y, color: instance_variable_get(color))
end

Then("pixel_at\\({var}, {int}, {int}\\) = {var}") do |canvas, x, y, color|
  assert_equal(instance_variable_get(canvas).pixel_at(x: x, y: y), instance_variable_get(color))
end

When("{var} ← canvas_to_ppm\\({var}\\)") do |name, canvas|
  instance_variable_set(name, instance_variable_get(canvas).to_ppm)
end

Then("lines 1-3 of {var} are ok") do |name|
  parts = instance_variable_get(name).split("\n")
  assert_equal(parts[0], "P3")
  assert_equal(parts[1], "5 3")
  assert_equal(parts[2], "255")
end

Then("lines 4-6 of {var} are ok") do |name|
  parts = instance_variable_get(name).split("\n")
  assert_equal(parts[3], "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0")
  assert_equal(parts[4], "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0")
  assert_equal(parts[5], "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255")
end

Then("{var} ends with a newline character") do |name|
  assert_equal(instance_variable_get(name).end_with?("\n"), true)
end

Then("canvas {var}.{word} = {float}") do |name, attribute, value|
  assert_equal(instance_variable_get(name).send(attribute), value)
end
