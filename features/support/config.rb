require "minitest"
World(Minitest::Assertions)

ParameterType(
  name: "var",
  regexp: /[a-zA-Z][0-9]?|zero|norm|red|ppm|transform|scaling|inv|half_quarter|full_quarter/,
  transformer: ->(match) { "@#{match}".to_sym },
  use_for_snippets: false
)

# ParameterType(
#   name: "tvar",
#   # regexp: /[a-z][1-4]?|zero|norm|red|origin|direction|intensity|position|light|eyev|normalv|light/,
#   regexp: /[a-z][1-4]/,
#   transformer: ->(match) { "@#{match}".to_sym },
#   use_for_snippets: false
# )

ParameterType(
  name: "tuple",
  regexp: /tuple\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\s*\)/,
  transformer: ->(x, y, z, w) { Rayz::Tuple.new(x: x.to_f, y: y.to_f, z: z.to_f, w: w.to_f) }
)

ParameterType(
  name: "point",
  regexp: /point\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\)/,
  transformer: ->(x, y, z) { Rayz::Point.new(x: x.to_f, y: y.to_f, z: z.to_f) }
)

ParameterType(
  name: "vector",
  regexp: /vector\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\)/,
  transformer: ->(x, y, z) { Rayz::Vector.new(x: x.to_f, y: y.to_f, z: z.to_f) }
)

ParameterType(
  name: "canvas",
  regexp: /canvas\(([0-9]+),\s*([0-9]+)\)/,
  transformer: ->(w, h) { Rayz::Canvas.new(width: w.to_i, height: h.to_i) }
)

ParameterType(
  name: "color",
  regexp: /color\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\)/,
  transformer: ->(r, g, b) { Rayz::Color.new(red: r.to_f, green: g.to_f, blue: b.to_f) }
)

ParameterType(
  name: "translation",
  regexp: /translation\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\)/,
  transformer: ->(x, y, z) { Rayz::Matrix.translation(x.to_f, y.to_f, z.to_f) }
)

ParameterType(
  name: "scaling",
  regexp: /scaling\(([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+),\s*([-+]?[0-9]*\.?[0-9]+)\)/,
  transformer: ->(x, y, z) { Rayz::Matrix.scaling(x.to_f, y.to_f, z.to_f) }
)

ParameterType(
  name: "rotation",
  regexp: /rotation_[xyz]/,
  transformer: ->(match) { match.to_sym }
)
