# frozen_string_literal: true

require_relative "lib/rayz/version"

Gem::Specification.new do |spec|
  spec.name = "rayz"
  spec.version = Rayz::VERSION
  spec.authors = ["Craig Treptow"]
  spec.email = ["craig.treptow@gmail.com"]

  spec.summary = "An implentation of the Ray Tracer challeng."
  spec.description = ""
  spec.homepage = "https://gitlab.com/CraigTreptow/rayz"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.2.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/CraigTreptow/rayz"
  spec.metadata["changelog_uri"] = "https://gitlab.com/CraigTreptow/rayz/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "cucumber", "~> 8.0"
  spec.add_dependency "debug"
  spec.add_dependency "dry-cli", "~> 1.0"
  spec.add_dependency "matrix", "~> 0.4"
  spec.add_dependency "minitest", "~> 5.0"
  spec.add_dependency "minitest-red_green", "~> 0.9"
  spec.add_dependency "rake", "~> 13.0"
  spec.add_dependency "solargraph", "~> 0.48"
  spec.add_dependency "solargraph-standardrb", "~> 0.0"
  spec.add_dependency "standard", "~> 1.3"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
