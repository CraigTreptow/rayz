# rayz

An implementation of the ray tracer described in [_The Ray Tracer Challenge_](http://raytracerchallenge.com/)

## Chapters
- 1 -> `bin/rayz chapter1`
- 2 -> `bin/rayz chapter1 && cp output/chapter2.ppm /mnt/c/Users/craig/Documents`
  - Now open with GIMP (on Windows)
- 3 -> ?

## CI Verification

[![Coverage Status](https://coveralls.io/repos/gitlab/CraigTreptow/rayz/badge.svg?branch=main)](https://coveralls.io/gitlab/CraigTreptow/rayz?branch=main)


## Background Information

A left-handed coordinate system is used here.

The y axis goes up, the x axis points to the right, and the z axis points away from you.

```
-x     +y
  \     |
   \    |
    \   |
     \  |
      \ |
-z-----\------------+z
        \
        |\
        | \
        |  \
        |   \
        |    \
       -y    +x
```

## Questions

- ?
## Notes

I was getting this error when trying to run `cucumber`:

```
 ~/src/rayz | on main *1 !2 ?1  bundle exec cucumber                                                                                                                                 1 err | at 04:00:28 PM 
bundler: failed to load command: cucumber (/home/craig/.asdf/installs/ruby/3.2.0/bin/cucumber)
/home/craig/.asdf/installs/ruby/3.2.0/lib/ruby/gems/3.2.0/gems/ffi-1.15.5/lib/ffi.rb:5:in `require': libffi.so.8: cannot open shared object file: No such file or directory - /home/craig/.asdf/installs/ruby/3.2.0/lib/ruby/gems/3.2.0/gems/ffi-1.15.5/lib/ffi_c.so (LoadError)
```

To resolve, I had to do the following:

`gem install ffi --version "1.15.5" -- --disable-system-libffi`

This was under WSL2 & Ubuntu 20.04.  See the answers to [this question.](https://stackoverflow.com/questions/70810903/libffi-so-8-cannot-open-shared-object-file)


### Chapter1 - Tuples, Points, and Vectors

- `bin/rayz chapter1`

### Chapter 2 - 

- `bin/rayz chapter1 && cp output/chapter2.ppm /mnt/c/Users/craig/Documents`
- Now open with GIMP (on Windows)
