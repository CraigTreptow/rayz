# frozen_string_literal: true

require "matrix"
Dir[File.join(__dir__, "rayz", "*.rb")].each { |file| require file }
Dir[File.join(__dir__, "core_extensions", "matrix", "*.rb")].each { |file| require file }

module Rayz
  class Error < StandardError; end
  # Your code goes here...
end
