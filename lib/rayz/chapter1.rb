# frozen_string_literal: true

require "rayz/tuple"
require "rayz/point"
require "rayz/vector"
require "rayz/util"
require "rayz/version"

module Rayz
  class Chapter1
    def self.tick(environment, projectile)
      position = projectile[:position]
      velocity = projectile[:velocity]
      gravity = environment[:gravity]
      wind = environment[:wind]

      puts sprintf("Projectile at x: %5.2f, y: %5.2f, z: %5.2f", position.x, position.y, position.z)

      return if position.y <= 0

      new_position = position + velocity
      new_velocity = velocity + gravity + wind
      projectile = {
        position: new_position,
        velocity: new_velocity
      }

      tick(environment, projectile)
    end
  end
end
