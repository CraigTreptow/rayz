# frozen_string_literal: true

module Rayz
  class Vector < Tuple
    attr_accessor :x, :y, :z
    attr_reader :w

    def initialize(x:, y:, z:)
      @x = x
      @y = y
      @z = z
      @w = 0
    end

    def magnitude
      Math.sqrt(@x**2 + @y**2 + @z**2)
    end

    def normalize
      m = magnitude
      Rayz::Vector.new(
        x: x / m,
        y: y / m,
        z: z / m
      )
    end

    def dot(other)
      x * other.x +
        y * other.y +
        z * other.z +
        w * other.w
    end

    def cross(other)
      # order matters!
      # a.cross(b) != b.cross(a)
      # the vector changes direction
      # X.cross(Y) gives you Z, but Y.cross(X)gives you -Z!

      Rayz::Vector.new(
        x: y * other.z - z * other.y,
        y: z * other.x - x * other.z,
        z: x * other.y - y * other.x
      )
    end
  end
end
