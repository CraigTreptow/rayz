module Rayz
  class Canvas
    attr_accessor :grid
    attr_reader :width, :height

    def initialize(width:, height:, raise_on_errors: true)
      @raise_on_errors = raise_on_errors
      @width = width
      @max_width = @width - 1
      @height = height
      @max_height = @height - 1
      @grid = initialize_grid
    end

    def to_ppm
      [ppm_header, ppm_body, "\n"].flatten.join("\n")
    end

    def pixel_at(x:, y:)
      raise "x: #{x} is out of bounds.  max: #{@max_width}" if x > @max_width && @raise_on_errors
      raise "y: #{y} is out of bounds.  max: #{@max_height}" if y > @max_height && @raise_on_errors

      @grid[x][y]
    end

    def write_pixel(x:, y:, color:)
      raise "x: #{x} is out of bounds.  max: #{@max_width}" if x > @max_width && @raise_on_errors
      raise "y: #{y} is out of bounds.  max: #{@max_height}" if y > @max_height && @raise_on_errors

      @grid[x][y] = color
    end

    def to_s
      output = " width: #{@width}  (0 <-> #{@width - 1})\nheight: #{@height}  (0 <-> #{@height - 1})\n"

      @height.times do |height_index|
        @width.times do |width_index|
          output << "[w: #{width_index} h: #{height_index} - #{@grid[width_index][height_index]}] "
        end
        output << "\n"
      end

      output
    end

    private

    def ppm_header
      ["P3", "#{@width} #{@height}", "255"]
    end

    def ppm_body
      result = []

      @height.times do |height_index|
        row = ""
        @width.times do |width_index|
          row << "#{clamp(@grid[width_index][height_index].red)} #{clamp(@grid[width_index][height_index].green)} #{clamp(@grid[width_index][height_index].blue)} "
        end
        result << row.strip
      end

      result
    end

    def initialize_grid
      color = Rayz::Color.new(red: 0, green: 0, blue: 0)
      Array.new(@width) { Array.new(@height) { color } }
    end

    def clamp(color_value, min_value = 0, max_value = 255)
      adjusted_value = color_value * (max_value + 0.99)

      return 0 if adjusted_value < min_value
      return 255 if adjusted_value > max_value
      adjusted_value.round
    end
  end
end
