# frozen_string_literal: true

module Rayz
  class Tuple
    TOLERANCE = 0.0001
    attr_accessor :x, :y, :z, :w

    def initialize(x:, y:, z:, w:)
      @x = x
      @y = y
      @z = z
      @w = w
    end

    def point?
      # w == 1.0
      (w - 1.0).abs < TOLERANCE
    end

    def vector?
      # w == 0
      (w - 0).abs < TOLERANCE
    end

    def +(other)
      Rayz::Tuple.new(
        x: x + other.x,
        y: y + other.y,
        z: z + other.z,
        w: w + other.w
      )
    end

    def -(other)
      Rayz::Tuple.new(
        x: x - other.x,
        y: y - other.y,
        z: z - other.z,
        w: w - other.w
      )
    end

    def -@
      Rayz::Tuple.new(
        x: -x,
        y: -y,
        z: -z,
        w: -w
      )
    end

    def *(other)
      Rayz::Tuple.new(
        x: x * other,
        y: y * other,
        z: z * other,
        w: w * other
      )
    end

    def /(other)
      Rayz::Tuple.new(
        x: x / other,
        y: y / other,
        z: z / other,
        w: w / other
      )
    end

    def ==(other)
      Rayz::Util.equal?(x, other.x) &&
        Rayz::Util.equal?(y, other.y) &&
        Rayz::Util.equal?(z, other.z) &&
        Rayz::Util.equal?(w, other.w)
    end
  end
end
