# frozen_string_literal: true

module Rayz
  class Point < Tuple
    attr_accessor :x, :y, :z
    attr_reader :w

    def initialize(x:, y:, z:)
      @x = x
      @y = y
      @z = z
      @w = 1.0
    end
  end
end
