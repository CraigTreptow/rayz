module Rayz
  class Matrix < Matrix
    def self.scaling(x, y, z)
      matrix =
        [
          [x, 0, 0, 0],
          [0, y, 0, 0],
          [0, 0, z, 0],
          [0, 0, 0, 1]
        ]
      build(4, 4) { |i, j| matrix.dig(i, j).to_f }
    end

    def self.translation(x, y, z)
      matrix =
        [
          [1, 0, 0, x],
          [0, 1, 0, y],
          [0, 0, 1, z],
          [0, 0, 0, 1]
        ]
      build(4, 4) { |i, j| matrix.dig(i, j).to_f }
    end

    def self.rotation_x(r)
      matrix =
        [
          [1, 0, 0, 0],
          [0, Math.cos(r), -Math.sin(r), 0],
          [0, Math.sin(r), Math.cos(r), 0],
          [0, 0, 0, 1]
        ]
      build(4, 4) { |i, j| matrix.dig(i, j) }
    end

    def self.rotation_y(r)
      matrix =
        [
          [Math.cos(r), 0, Math.sin(r), 0],
          [0, 1, 0, 0],
          [-Math.sin(r), 0, Math.cos(r), 0],
          [0, 0, 0, 1]
        ]
      build(4, 4) { |i, j| matrix.dig(i, j) }
    end

    def self.rotation_z(r)
      matrix =
        [
          [Math.cos(r), -Math.sin(r), 0, 0],
          [Math.sin(r), Math.cos(r), 0, 0],
          [0, 0, 1, 0],
          [0, 0, 0, 1]
        ]
      build(4, 4) { |i, j| matrix.dig(i, j) }
    end

    def ==(other)
      (0..rows.count - 1).each do |x|
        (0..column_count - 1).each do |y|
          return false unless Rayz::Util.equal?(self[x, y], other[x, y])
        end
      end

      true
    end

    def times_tuple(t)
      matrix_from_tuple = Rayz::Matrix[[t.x], [t.y], [t.z], [t.w]]
      r = self * matrix_from_tuple
      ra = r.to_a
      Rayz::Tuple.new(x: ra[0][0], y: ra[1][0], z: ra[2][0], w: ra[3][0])
    end

    def times_point(p)
      matrix_from_point = Rayz::Matrix[[p.x], [p.y], [p.z], [1.0]]
      r = self * matrix_from_point
      ra = r.to_a
      Rayz::Point.new(x: ra[0][0], y: ra[1][0], z: ra[2][0])
    end
  end
end
