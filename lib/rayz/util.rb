# frozen_string_literal: true

module Rayz
  class Util
    TOLERANCE = 0.0001

    def self.equal?(f, g)
      (f - g).abs < TOLERANCE
    end
  end
end
