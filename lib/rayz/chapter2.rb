# frozen_string_literal: true

require "rayz/tuple"
require "rayz/point"
require "rayz/vector"
require "rayz/util"
require "rayz/version"

module Rayz
  class Chapter2
    def self.tick(environment, projectile, color, canvas)
      position = projectile[:position]
      velocity = projectile[:velocity]
      gravity = environment[:gravity]
      wind = environment[:wind]

      y_to_plot = canvas.height - position.y.round(0)
      puts sprintf("Plot x: %2d, y: %2d, z: %2d", position.x.round(0), y_to_plot, position.z.round(0))

      canvas.write_pixel(x: position.x.round(0), y: y_to_plot, color: color)

      return canvas if position.y <= 0

      new_position = position + velocity
      new_velocity = velocity + gravity + wind
      projectile = {
        position: new_position,
        velocity: new_velocity
      }

      tick(environment, projectile, color, canvas)
    end
  end
end
