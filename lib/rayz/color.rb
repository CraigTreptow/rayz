# frozen_string_literal: true

module Rayz
  class Color
    attr_accessor :red, :green, :blue

    def initialize(red:, green:, blue:)
      @red = red
      @green = green
      @blue = blue
    end

    def +(other)
      Rayz::Color.new(
        red: red + other.red,
        green: green + other.green,
        blue: blue + other.blue
      )
    end

    def -(other)
      Rayz::Color.new(
        red: red - other.red,
        green: green - other.green,
        blue: blue - other.blue
      )
    end

    def *(other)
      return hadamard_product(other) if other.instance_of?(Rayz::Color)

      multiply_by_scalar(other)
    end

    def ==(other)
      Rayz::Util.equal?(red, other.red) &&
        Rayz::Util.equal?(green, other.green) &&
        Rayz::Util.equal?(blue, other.blue)
    end

    def to_s
      sprintf("r: %5.2f, g: %5.2f, b: %5.2f", red, green, blue)
    end

    private

    def multiply_by_scalar(scalar)
      Rayz::Color.new(
        red: red * scalar,
        green: green * scalar,
        blue: blue * scalar
      )
    end

    def hadamard_product(other)
      Rayz::Color.new(
        red: red * other.red,
        green: green * other.green,
        blue: blue * other.blue
      )
    end
  end
end
