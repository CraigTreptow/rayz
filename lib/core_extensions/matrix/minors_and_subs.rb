require "matrix"

module CoreExtensions
  module Matrix
    module MinorsAndSubs
      def submatrix(r, c)
        first_minor(r, c)
      end

      def mynor(r, c)
        submatrix(r, c).determinant
      end
    end
  end
end

Matrix.include(CoreExtensions::Matrix::MinorsAndSubs)
