require "matrix"

module CoreExtensions
  module Matrix
    module Inversion
      def invertible?
        !determinant.zero?
      end
    end
  end
end

Matrix.include(CoreExtensions::Matrix::Inversion)
